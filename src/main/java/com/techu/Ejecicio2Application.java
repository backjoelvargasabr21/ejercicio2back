package com.techu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejecicio2Application {

	public static void main(String[] args){
		SpringApplication.run(Ejecicio2Application.class, args);
	}

}
