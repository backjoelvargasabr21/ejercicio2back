package com.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("ProductosJoelVargas")
public class ProductoMongo {

    public String id;
    public String nombre;
    public Double precio;

    public ProductoMongo() {
    }

    public ProductoMongo(String nombre, Double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

}

