package com.techu;

import com.techu.data.ProductoMongo;
import com.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class controller {

    @Autowired
    private ProductoRepository repository;

    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }

    @GetMapping(value = "/productos/{id}", produces = "application/json")
    public ResponseEntity<String> obtenerListadoID(@PathVariable String id){
        Optional<ProductoMongo> resultado = repository.findById(id);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    @PostMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<String> postProductos(@RequestBody ProductoMongo newProducto){
        ProductoMongo resultado = repository.insert(newProducto);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    @PutMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<String> putProductos(@RequestBody ProductoMongo newProducto){
        ProductoMongo resultado = repository.save(newProducto);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    @DeleteMapping(value="/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }

}

